
import selenium.webdriver as webdr
import time, bs4, datetime, os, sys

def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.dirname(__file__)
    return os.path.join(base_path, relative_path)

def start_program (user, passw, desk, exec_nums):
    driver = webdr.Chrome(resource_path ('./driver/chromedriver.exe'))
    for i in range (0, exec_nums):
        if desk == '1':
            driver.get('https://jira.uwaterloo.ca/projects/ISTSD/queues/custom/51') # 打开网页
        else:
            driver.get ('https://jira.uwaterloo.ca/projects/RESHELP/queues/custom/96')
        time.sleep(1)

        today = datetime.date.today()
        print("\nToday's date:", today)
        if i == 0:
            #finds the username field and put in  login name
            username = driver.find_element_by_id ('login-form-workaccount')
            username.send_keys (user + '@uwaterloo.ca')
            #finds the next button
            next_but = driver.find_element_by_id ('loginMainButton')
            next_but.click()
            time.sleep (0.5)
            #enters the password to JIRA
            passwo = driver.find_element_by_id ('passwordInput')
            passwo.send_keys (passw)
            next_but = driver.find_element_by_id ('submitButton')
            next_but.click()
        time.sleep (15)

        #finds the issue link and enters them here
        res = driver.find_elements_by_class_name ("issue-link")
        #below for loop filters out the ticket number clean. gets rid of the extra stuff 
        #also appends the issue link into the driver
        URLs = []
        #URLs contains urls to the individual ticket pages
        print ("Please wait for approx. 20 secs to have HTML data loaded.")

        for r in res:
            # print (r.text)
            if 'ISTSD-' in r.text or 'RESHELP-' in r.text:
                # print (r.text)
                URLs.append ("https://jira.uwaterloo.ca/browse/" + r.text)
                time.sleep (0.5)

        #RESOLUTION_URLS stores the links to resolution of all qualifying stale tickets
        RESOLUTION_URLS = []
        for URL in URLs:
            print (URL)
            try:
                driver.get (URL)
                time.sleep (2)
                #dates is the 2 dates (created and updated)
                dates = driver.find_elements_by_class_name ('livestamp')
                #last_upd is the time of last updated to a specific ticket
            
                last_upd = dates[-1].text
            except:
                continue
            # print (last_upd)
            #Filters out tickets that have been updated within the past week (7 days) as required 
            if ('Yesterday' in last_upd) or ('minute' in last_upd) or ('hour' in last_upd) or ('now' in last_upd) or ('day' in last_upd):
                continue
            wrk_stat = driver.find_element_by_id ('status-val').text

            wrk_stat = wrk_stat.lower()
            print (wrk_stat)
            #Takes care of issues that are "In Progress"
            if wrk_stat == 'in progress' or wrk_stat == 'escalated' or wrk_stat == 'waiting for support':
                # pag = driver.page_source
                # html = bs4.BeautifulSoup (pag, 'html.parser')
                # #finds the html element with the link to resolution
                # link = html.find ('a', id = 'action_id_761') ['href']
                # print (link)
                # RESOLUTION_URLS.append (link)
                continue
            try:
                #clicks on the Workflow button
                workflow_button = driver.find_element_by_id ('opsbar-transitions_more')
                workflow_button.click()
                pageSource = driver.page_source
                soup = bs4.BeautifulSoup(pageSource,'html.parser')
                #finds the url to the specific issue resolution page (with the text box)
                to_resolv = soup.find('aui-item-link', class_ = 'issueaction-workflow-transition') ['href']
                print (to_resolv)
                RESOLUTION_URLS.append ("https://jira.uwaterloo.ca" + to_resolv)
            except:
                continue

        print ("\nAlmost there. Please be patient")
        email_message = ''' 
        Hi there,

        Thank you for reaching out to the IST Service Desk. We have not heard back from you for a while, so this ticket will be resolved. If you still require assistance, please reply to this email to re-open this ticket.

        Kind regards,

        Service Desk Specialist

        Information Systems & Technology
        East Campus 2, University Of Waterloo, Ontario, Canada
        Phone: 519-888-4567 x44357
        Email: helpdesk@uwaterloo.ca
        Live chat: https://bit.ly/ist-livechat
        '''                 
        # print (email_message)
        # print (RESOLUTION_URLS)
        for url in RESOLUTION_URLS:
            driver.get(url)
            time.sleep (1)
            text = driver.find_element_by_xpath('//li[@data-mode="source"]//a[@href="#"]')
            time.sleep (0.5)
            text.location_once_scrolled_into_view
            text.click()
            input_field = driver.find_element_by_id ('comment')
            input_field.send_keys (email_message)
            resol_button = driver.find_element_by_id ('issue-workflow-transition-submit')
            resol_button.click()

    driver.close()
    time.sleep (1)
    return

# def diff_dates(date1, date2):
#     # if 'week' in date1:
#     #     return abs(date2 - datetime.timedelta (days=7))).days
#     return abs(date2-date1).days

def main():
    time.sleep (1)
    print ("Thanks for using this automation script. Last revised on July 25, 2020. \n Program starting. \n")
    time.sleep (2)
    choice = input ('Please pick the desk; 1 for ISTSD or 2 for RESHELP (enter digit only): ')
    time.sleep (0.5)
    num_of_tickets = int (input("Enter the total number of stale tickets at that desk: "))
    time.sleep (0.5)
    watiam_username = input ("Enter your WatIAM usermame: ")
    time.sleep (1)
    password = input ("Your password is stored to your local disk. Enter your WatIAM password: ")
    time.sleep (1.5)
    exec_num = int(num_of_tickets / 50) + 1
    start_program(watiam_username, password, choice, exec_num)
    print ("Program terminated. Thanks for using this automation script! Goodbye!")

main()